import { Component } from '@angular/core';
import {I18nLanguageService} from "./i18n-language.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public readonly supportedLanguages = I18nLanguageService.SUPPORTED_LANGUAGES;

  constructor(private i18nLanguageService: I18nLanguageService) {
  }

  changeLanguage(language: string): void {
    this.i18nLanguageService.changeLanguage(language);
  }

  isCurrentLang(language: string): boolean {
    return this.i18nLanguageService.isCurrentLanguage(language);
  }

  changedRouteActivate($event: any) {
    window.scrollTo(0, 0);
  }
}
