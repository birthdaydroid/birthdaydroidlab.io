import {catchError} from "rxjs/operators";
import {Observable, of} from "rxjs";
import {TranslateService} from "@ngx-translate/core";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class I18nLanguageService {
  public static readonly SUPPORTED_LANGUAGES = ['en', 'de']
  public static readonly DEFAULT_LANGUAGE = 'en'

  private static LOCAL_STORAGE_LANGUAGE_SETTING_KEY = 'lang-setting';

  constructor(private translateService: TranslateService) {
  }


  public initLanguageService(): Promise<boolean> {
    this.translateService.setDefaultLang(I18nLanguageService.DEFAULT_LANGUAGE)

    let lang = localStorage.getItem(I18nLanguageService.LOCAL_STORAGE_LANGUAGE_SETTING_KEY);

    if (!lang) {
      lang = this.translateService.getBrowserLang();
    }

    if (!I18nLanguageService.SUPPORTED_LANGUAGES.includes(lang)) {
      lang = this.translateService.getDefaultLang();
    }

    return new Promise<boolean>((resolve) => {
      this.translateService.use(lang)
        .pipe(catchError(() => of(undefined)))
        .subscribe(() => resolve(true));
    });
  }

  public changeLanguage(language: string): Observable<any> {
    if (language === this.translateService.getBrowserLang()) {
      localStorage.removeItem(I18nLanguageService.LOCAL_STORAGE_LANGUAGE_SETTING_KEY);
    } else {
      localStorage.setItem(I18nLanguageService.LOCAL_STORAGE_LANGUAGE_SETTING_KEY, language);
    }

    return this.translateService.use(language)
  }

  public isCurrentLanguage(language: string): boolean {
    return this.translateService.currentLang === language;
  }
}
