import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContributeRoutingModule } from './contribute-routing.module';
import { ContributeComponent } from './contribute.component';
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
  declarations: [ContributeComponent],
  imports: [
    CommonModule,
    ContributeRoutingModule,
    TranslateModule.forChild()
  ]
})
export class ContributeModule { }
