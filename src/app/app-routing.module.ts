import {NgModule} from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';

const routes: Routes = [{
  path: '', redirectTo: 'home', pathMatch: 'full'
}, {
  path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
}, {
  path: 'features', loadChildren: () => import('./features/features.module').then(m => m.FeaturesModule)
}, {
  path: 'contribute', loadChildren: () => import('./contribute/contribute.module').then(m => m.ContributeModule)
}, {
  path: 'download', loadChildren: () => import('./download/download.module').then(m => m.DownloadModule)
}, {
  path: '**', redirectTo: 'home'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules,
    useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
